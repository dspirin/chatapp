(function() {
    'use strict';

	angular
	.module("app")
	.factory("Chat", ["$firebaseArray", 
		function($firebaseArray) {
	    	var messagesRef = firebase.database().ref().child("messages");
	    	var factory = {
	    		addMessage: addMessage,
	    		getMessages: getMessages,
	    		getData: getData,
	    		getLastData: getLastData
	    	};
	    	return factory;

	    	function addMessage(message) {
	    		factory.getMessages().$add(message);
	    	}
  			function getMessages() {
        		return $firebaseArray(messagesRef);
    		}
    		function getData() {
        		return $firebaseArray(messagesRef);
    		}
    		function getLastData() {
        		return $firebaseArray(messagesRef.limitToLast(30));
    		}
    	}
   	]);
})();