(function() {
    'use strict';

    angular
        .module("app")
        .directive("signIn", ['$location', 'Auth',
            function($location, Auth) {
            	return {
                    restrict: 'E',
                    templateUrl: 'app/signIn/signIn.html',
            		scope: {},
            		link: function(scope) {

                        init();

                        function init() {
                            var check = localStorage.getItem('chatName');
                            if (check) {
                                scope.email = check;
                            }
                        }
                        
                        scope.openChat = function() {
                            Auth.$signInWithEmailAndPassword(scope.email, scope.password)
                            .then(function(firebaseUser) {
                                localStorage.setItem('chatName', firebaseUser.email);
                                $location.path("/chat");
                            }).catch(function(error) {
                                scope.error = error.message;
                            });
                        };
            		}
            	};
    	    }
        ]);
})();