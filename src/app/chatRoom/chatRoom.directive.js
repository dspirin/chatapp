(function() {
    'use strict';

    angular
    .module("app")
    .directive("chatRoom", ['Auth', 'Chat', 'Data', '$timeout', '$interval',
        function(Auth, Chat, Data, $timeout, $interval) {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'app/chatRoom/chatRoom.html',
                link: function(scope) {
                    var el = document.querySelector('.chat');
                    var dataBaseUser = Data;
                    scope.user;
                    
                    Auth.$onAuthStateChanged(function(firebaseUser) {
                        scope.user = firebaseUser;
                    });
                    
                    scope.data = Chat.getLastData();
                    $timeout(function() {
                        el.scrollTop = el.scrollHeight;
                    }, 1500);

                    scope.sendMessage = function() {
                        var message = {
                            author: scope.user.email,
                            time: firebase.database.ServerValue.TIMESTAMP,
                            content: scope.text
                        };
                        Chat.addMessage(message);
                        scope.text = '';
                        scrollDown();
                    };

                    function scrollDown() {
                        $timeout(function() {
                            el.scrollTop = el.scrollHeight;
                        }, 10)
                    }

                    $interval(function() {
                        var index = Math.round(Math.random() * (dataBaseUser.length - 1));
                        Chat.addMessage(dataBaseUser[index]);
                        scrollDown();    
                    }, 7000);
                    
                }
            };
        }
    ]);
})();