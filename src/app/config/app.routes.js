(function() {
    'use strict';

    angular
        .module('app')
        .config(['$stateProvider', '$urlRouterProvider',
        	function($stateProvider, $urlRouterProvider) {
				$urlRouterProvider.otherwise('/');
				$stateProvider
    				.state("home", {
				    	url: '/',
				    	template: '<sign-in></sign-in>',
				    	resolve: {
        			        "currentAuth": ["Auth", function(Auth) {
						        return Auth.$waitForSignIn();
        					}]
      					}
    				})
					.state("chat", {
					    url: '/chat',
					    template: '<chat-room></chat-room>',
						resolve: {
        			        "currentAuth": ["Auth", function(Auth) {
						        return Auth.$requireSignIn();
        					}]
      					}
    				});
			}
		]);
})();
