var gulp = require('gulp'),
	server = require('gulp-server-livereload'),
	useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-clean-css'),
    cleanDest = require('gulp-clean-dest');
 
gulp.task('start', function() {
  gulp.src('src')
    .pipe(server({
      livereload: true,
      defaultFile: 'index.html',
      open: true
    }));
});

gulp.task('images', function () {
    return gulp.src('src/images/**/*')
        .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', function () {
    return gulp.src('src/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'));
});

gulp.task('views', function () {
    return gulp.src('src/app/**/*.html')
        .pipe(gulp.dest('dist/app'));
});

gulp.task('build', ['images', 'fonts', 'views'], function () {
    return gulp.src('src/*.html')
        .pipe(cleanDest('dist'))
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulp.dest('dist'));
});